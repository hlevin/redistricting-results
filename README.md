# Redistricting-Results

The block equivalency files are provided for all of the experimental tests. The folder name matches the algorithm version, and the file name FIPS code and two letter state code will identify the state. The block equivalency files contain the block/tract ID and the index of the district to which it was assigned. You can reconstruct districts and run your own assessments with this information.

